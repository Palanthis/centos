#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

echo "###################################"
echo "#### This must be run as root  ####"
echo "####   using su, NOT sudo     #####"
echo "###################################"

# Add EPEL Repos
rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm

# Install Build Essentials
yum install gcc kernel-devel kernel-headers dkms make bzip2 perl

# Install Xorg
yum groupinstall “X Window system”

# Install Xfce
yum groupinstall xfce -y

# Enable Autostart
echo "exec /usr/bin/xfce4-session" >> ~/.xinitrc

# Enable network time
systemctl enable systemd-networkd.service

systemctl start systemd-networkd.service

systemctl enable systemd-timesyncd.service

systemctl start systemd-timesyncd.service

timedatectl set-ntp true

hwclock --systohc --utc

echo " "
echo "Let's make sure everything looks right!"

timedatectl status
